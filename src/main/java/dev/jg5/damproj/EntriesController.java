package dev.jg5.damproj;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class EntriesController {
	
	private static final Logger logger = LoggerFactory.getLogger(EntriesController.class);
	
	/*
	 * curl -i -X POST -H 'content-type:application/json' -d '{"key":"4", "value":"four"}' http://localhost:8080/add
	 * curl -i http://localhost:8080/entries/4
	 * curl -i http://localhost:8080/entries
	 * 
	 */

	@Autowired
	private final EntriesRepository repository;

	public EntriesController(EntriesRepository repository) {
		this.repository = repository;
	}
	
	@GetMapping("/entries")
	public ResponseEntity<List<Entry<String, String>>> findAll() {		
		
		return ResponseEntity
				.status(HttpStatus.OK)
				.body(repository.findAll());
	}
	
	@GetMapping("/entries/{key}")
	public ResponseEntity<Entry<String, String>> findEntry(@PathVariable("key") String key) {		
		
		Entry<String, String> found = repository.findEntry(key);
		if (found == null) {
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body(null);
		}
		else {
			return ResponseEntity
					.status(HttpStatus.OK)
					.body(repository.findEntry(key));	
		}
	}
	
	@PostMapping("/add")
	public ResponseEntity<Void> addEntry(@RequestBody Entry<String, String> entry) {
		
		logger.info("post body is " + entry);		
		try {
			repository.addEntry(entry);
			return new ResponseEntity<>(HttpStatus.ACCEPTED);
			
		} catch (DuplicateException e) {
			return ResponseEntity.status(HttpStatus.CONFLICT).body(null);
		}		
	}
}
