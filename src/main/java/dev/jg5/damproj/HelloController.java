package dev.jg5.damproj;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloController {

    private static final Logger logger = LoggerFactory.getLogger(HelloController.class);
    
    static final int MAX = 4;
    int count = MAX;
	
	@GetMapping("/")
	public String index() {
		
		if (--count == 0) {
			count = MAX;
			throw new RuntimeException("is zero");
		}
		
		logger.debug("hello requested");
		return "Many, Many Greetings from Spring Boots!";
	}

}