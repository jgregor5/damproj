package dev.jg5.damproj;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.JdbcTemplate;

@Configuration
public class MainConfig {

	@Autowired
	JdbcTemplate jdbcTemplate;
	
	@Bean
	EntriesRepository entriesRepository() {
		return new EntriesRepositoryMySQL(jdbcTemplate);
	}

}
