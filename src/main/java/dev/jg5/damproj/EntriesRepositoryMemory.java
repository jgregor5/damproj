package dev.jg5.damproj;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class EntriesRepositoryMemory implements EntriesRepository {

	private Map<String, String> values;
	
	public EntriesRepositoryMemory() {
		values = new HashMap<>();
	}
	
	@Override
	public void addEntry(Entry<String, String> e) {
		values.put(e.getKey(), e.getValue());
	}

	@Override
	public Entry<String, String> findEntry(String key) {
		String value = values.get(key);
		if (value == null) {
			return null;
		}
		return new Entry<String, String>(key, value);
	}
	
	@Override
	public List<Entry<String, String>> findAll() {
		
		List<Entry<String, String>> list = new ArrayList<>();
		for (Map.Entry<String, String> entry: values.entrySet()) {
			list.add(new Entry<String, String>(entry.getKey(), entry.getValue()));
		}		
		return list;
	}

}
