package dev.jg5.damproj;

public class DuplicateException extends Exception {

	private static final long serialVersionUID = 1L;

	public DuplicateException(String message) {
		super(message);
	}
	
	public DuplicateException(Throwable t) {
		super(t);
	}
	
	public DuplicateException(String message, Throwable t) {
		super(message, t);
	}
	
}
