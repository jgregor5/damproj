package dev.jg5.damproj;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class Entry<K, V> {

	private K k;
	private V v;
	
	public Entry(K k, V v) {
		this.k = k;
		this.v = v;
	}
	
	public K getKey() { return k; }
	public void setKey(K k) { this.k = k; }
	public V getValue() { return v; }
	public void setValue(V v) { this.v = v; }
	
	@JsonIgnore
	public int getCode() { return hashCode(); }
	
	@Override
	public String toString() {
		return "Entry{" + k + "=" + v + "}";
	}
}
