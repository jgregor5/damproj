package dev.jg5.damproj;

import java.util.List;

public interface EntriesRepository {

	void addEntry(Entry<String, String> e) throws DuplicateException;
	
	Entry<String, String> findEntry(String key);
	
	List<Entry<String, String>> findAll();
}
