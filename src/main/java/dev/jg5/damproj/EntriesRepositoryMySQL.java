package dev.jg5.damproj;

import java.util.List;

import org.springframework.dao.DuplicateKeyException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

public class EntriesRepositoryMySQL implements EntriesRepository {
	
	private JdbcTemplate jdbcTemplate;
	private RowMapper<Entry<String, String>> rowMapper;
	
	public EntriesRepositoryMySQL(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
		this.rowMapper = (r, i) -> {			
			return new Entry<>(r.getString("key"), r.getString("value"));
		};
	}
	
	@Override
	public void addEntry(Entry<String, String> e) throws DuplicateException {
		
		String sql = "INSERT INTO entries (`key`, `value`) VALUES (?, ?)";
		try {
			jdbcTemplate.update(sql, e.getKey(), e.getValue());
		} catch (DuplicateKeyException ex) {
			throw new DuplicateException("adding entry for key: " + e.getKey(), ex);
		}		
	}
	
	@Override
	public Entry<String, String> findEntry(String key) {
		
		String sql = "SELECT * FROM entries WHERE `key` = ?";		
		try {
			return jdbcTemplate.queryForObject(sql, rowMapper, key);
		} catch (EmptyResultDataAccessException e) {
			return null;
		}
	}
	
	@Override
	public List<Entry<String, String>> findAll() {
		
		String sql = "SELECT * FROM entries";		
		return jdbcTemplate.query(sql, rowMapper);
	}

	
}
