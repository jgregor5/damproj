package dev.jg5.damproj;

import static org.hamcrest.CoreMatchers.is;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

import java.util.List;

import org.hamcrest.Matchers;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

@WebMvcTest(EntriesController.class)
class EntriesControllerTests {

	@Autowired
    private MockMvc mockMvc;
	
	@MockBean
	EntriesRepository entriesRepository;
	
	@Test
	void findAll() throws Exception {
		
		Entry<String, String> e1 = new Entry<>("1", "one");
		Entry<String, String> e2 = new Entry<>("2", "two");
		Entry<String, String> e3 = new Entry<>("3", "three");
		
		List<Entry<String, String>> entries = List.of(e1, e2, e3);
		
		// see https://github.com/json-path/JsonPath
		
		Mockito.when(entriesRepository.findAll()).thenReturn(entries);
		mockMvc.perform(MockMvcRequestBuilders
				.get("/entries")
				.contentType(MediaType.APPLICATION_JSON))
        		.andExpect(jsonPath("$", Matchers.hasSize(3)))
        		.andExpect(jsonPath("$[2].value", is("three")));
	}
}
