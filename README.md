# README

## Installation

1\. Clone the repository:
    
    git clone https://gitlab.com/jgregor5/damproj.git    
    
2\. Compile it and build the package:
    
    ./mvnw compile
    ./mvnw package
    
3\. Check that the server starts and works:
    
    java -jar target/damproj-0.0.1-SNAPSHOT.jar
    
4\. Create a service for it in /etc/systemd/system/damproj.service:
    
    [Unit]
    Description=DAM Project
    After=network.target
    
    [Service]
    User=julian
    TimeoutStartSec=0
    Type=simple
    KillMode=process
    WorkingDirectory=/home/julian/damproj
    ExecStart=/usr/bin/java -jar target/damproj-0.0.1-SNAPSHOT.jar
    Restart=always
    RestartSec=2
    
    [Install]
    WantedBy=multi-user.target
    
5\. Enable and start the service:
    
    sudo systemctl enable damproj
    sudo systemctl start damproj
    
6\. Make sure that the firewall is open for the application port.
This may be set in application.properties.
    
    sudo ufw status
    sudo ufw allow <PORT>

## Database

### MySQL

Install the server:

	sudo apt install mysql-server
	sudo systemctl start mysql.service
	sudo mysql_secure_installation

Create the database from mysql:

	sudo mysql	
	
	> CREATE SCHEMA `dam2db`;
	> CREATE USER 'youruser'@'localhost' IDENTIFIED BY 'yourpassword';
	> GRANT ALL PRIVILEGES ON dam2db.* TO 'youruser'@'localhost';

	> CREATE TABLE `entries` (
	`key` varchar(100) NOT NULL,
	`value` varchar(100) NOT NULL,
	PRIMARY KEY (`key`)
	) ENGINE=InnoDB;

Add to your application.properties (the one in gitignore):

	spring.datasource.url=jdbc:mysql://localhost:3306/dam2db
	spring.datasource.username=youruser
	spring.datasource.password=yourpassword
	spring.datasource.driver-class-name=com.mysql.cj.jdbc.Driver

### PostgreSQL

Install the server:

	sudo apt install postgresql postgresql-contrib
	sudo -i -u postgres

Create the database:

	psql	
	
	> CREATE DATABASE dam2db;
	> CREATE USER youruser WITH ENCRYPTED PASSWORD 'yourpassword';
	> GRANT ALL PRIVILEGES ON DATABASE dam2db TO youruser;
		
	> CREATE TABLE public.entries (
    	key character varying NOT NULL,
	value character varying NOT NULL );
	> ALTER TABLE public.entries OWNER TO youruser;
	> ALTER TABLE ONLY public.entries
    	ADD CONSTRAINT entries_pk PRIMARY KEY (key);

Add to your application.properties (the one in gitignore):

	spring.datasource.url=jdbc:postgresql://localhost:5432/dam2db
	spring.datasource.username=youruser
	spring.datasource.password=yourpassword
	spring.datasource.driver-class-name=org.postgresql.Driver	


## Migrations

Copy default environment properties:

	cp migrations/environments/development.properties.default migrations/environments/development.properties

Edit development.properties and set the right URL and user/password.
Then, check these commands:

	mvn migration:status # this should show two pending migrations (create changelog and first migration)
	mvn migration:up # this will create the changelog and the entries tables (the two migrations)
	mvn migration:new -Dmigration.description=some_description # this will create a new empty migration

## Development

### Local Eclipse

You can create an Eclipse Maven project by following these steps:

*	In Git Repositories, clone this project and add it.
*	Import it from File > Import projects...
*	You should be able to run DamprojApplication.java.
*	You could push git changes and pull them from the server.

### Server Updates

Everytime you need to update the application, do:

	git pull
    ./mvnw compile
    ./mvnw package
    sudo systemctl restart damproj
    
You may check the logging (tail) using:

    tail -f logs/damproj.log
    # or
    journalctl -fu damproj
